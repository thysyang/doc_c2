# doc_c2

#### 介绍
学习的书

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明
- 软件设计师教程
	《代码精进之路 从码农到工匠》pdf电子书免费下载	
- 图解数据结构:使用C++
- C语言解惑:指针、数组、函数和多文件编程
- 算法图解
- 嵌入式LINUX基础教程(第2版)
- Photoshop CS6完全自学教程 综合案例
- C++设计新思维
- C语言项目开发实战入门
- Essential C++中文版
- C++ Primer中文版（第5版）
- Effective C++：改善程序与设计的55个具体做法（第三版）中文版
- More Effective C++：35个改善编程与设计的有效方法（中文版）
- 《C++标准库（第2版）
- C++面向对象多线程编程
- 网络多人游戏架构与编程
- 构建嵌入式LINUX系统
- 程序员的三门课：技术精进、架构修炼、管理探秘
- 笨办法学Linux
- Qt及Linux操作系统窗口设计
- 妙趣横生的算法(C语言实现 第2版)
- C语言实用之道
- 程序设计与数据结构.周立功
- C++沉思录
- 提高C++性能的编程技术
- C++实践之路
- 梦断代码
- C++ 高级参考手册
- 程序员修炼之道：从小工到专家
- Effective C++中文
- 大师谈游戏设计:创意与节奏
- 游戏人工智能编程案例精粹

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
